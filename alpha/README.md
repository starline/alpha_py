### ASP

**Alpha Serial Protocol (ASP)** - протокол, использующийся для передачи данных между бортовым вычислителем и автомобилем с установленным ПАК Alpha по USB. Подробная информация доступна на [соответствующей странице документации Alpha](https://gitlab.com/starline/alpha/-/blob/master/docs/serial.md).
